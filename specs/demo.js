import supertest from 'supertest';

function sum(a, b) {
    return a + b;
}

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});

test('Demo API', async () => {
    const { status } = await supertest('http://apichallenges.herokuapp.com')
        .post('/challenger')
        .set('Accept', 'application/json')
    expect(status).toBe(201);
});
